import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.chart.*
import javafx.scene.control.*
import javafx.scene.control.Alert.AlertType
import javafx.scene.input.KeyEvent


/**
 * Created by Paul Chernenko on 30.10.2017.
 */
class MainController {

    @FXML
    lateinit var chart : LineChart<Double, Double>

    @FXML
    lateinit var countOfSources: TextField

    @FXML
    lateinit var tau: TextField

    @FXML
    lateinit var roIntensity: TextField

    @FXML
    lateinit var lambdaIntensity: TextField

    @FXML
    lateinit var buildGraphButton : Button

    @FXML
    lateinit var tOtP : RadioButton

    @FXML
    lateinit var tOtM : RadioButton

    var countOfSourcesValue = 0
    var tauValue = 0
    var roIntensityValue = 0.0
    var lambdaIntensityValue = 0.0


    val integerEventHandler : EventHandler<KeyEvent> = EventHandler { event ->
        if (!event.character.matches(Regex("\\d*"))) {
            event.consume();
        }
    }

    val doubleEventHandler : EventHandler<KeyEvent> = EventHandler { event ->
        if(!event.character.matches(Regex("[0-9.]"))){
            event.consume();
        } else {
            val sourse = event.source as TextField
            if(sourse.text.contains(".") && event.character.matches(Regex("[.]"))){
                event.consume();
            } else if(sourse.text.isEmpty() && event.character.matches(Regex("[.]"))){
                event.consume();
            }
        }
    }

    fun initialize() {
        countOfSources.addEventFilter(KeyEvent.KEY_TYPED, integerEventHandler)
        tau.addEventFilter(KeyEvent.KEY_TYPED, integerEventHandler)
        roIntensity.addEventFilter(KeyEvent.KEY_TYPED, doubleEventHandler)
        lambdaIntensity.addEventFilter(KeyEvent.KEY_TYPED, doubleEventHandler)

        buildGraphButton.setOnAction {buildGraph()}

        val toggleGroup = ToggleGroup()
        tOtM.toggleGroup = toggleGroup
        tOtP.toggleGroup = toggleGroup

    }


    fun buildGraph() {
        try{
            countOfSourcesValue = countOfSources.text.toInt()
            tauValue = tau.text.toInt()
            roIntensityValue = roIntensity.text.toDouble()
            lambdaIntensityValue = lambdaIntensity.text.toDouble()
        } catch (e : Exception){
            val alert = Alert(AlertType.WARNING)
            alert.title = "Увага"
            alert.headerText = "Увага"
            alert.contentText = "Перевірте правильність введених даних"

            alert.showAndWait()
            return
        }

        var muIntensity : Double
        val p0 = DoubleArray(10)
        val eN = DoubleArray(10)
        val sumMultiplyN = DoubleArray(10)
        val sumMultiplyNForP0 = DoubleArray(10)

        if (tOtP.isSelected){

            val series = XYChart.Series<Double, Double>()

            var number = 0
            var multiply : Double

            for (i in 1..10){
                val ro : Double = i/ 10.0;
                muIntensity = lambdaIntensityValue / ro;


                for (n in 1..countOfSourcesValue){
                    multiply = 1.0

                    for (j in 0 until n) {
                        multiply *= Math.exp(lambdaIntensityValue * (countOfSourcesValue * tauValue + j / muIntensity)) - 1
                    }

                    sumMultiplyNForP0[number] += multiply
                }

                p0[number] = 1/(1 + sumMultiplyNForP0[number])

                for (n in 1 until countOfSourcesValue){
                    multiply = 1.0

                    for (j in 0..n){
                        multiply *= Math.exp(lambdaIntensityValue * (countOfSourcesValue * tauValue + j / muIntensity)) - 1
                    }
                    sumMultiplyN[number] += multiply;

                }

                eN[number] = countOfSourcesValue * tauValue + countOfSourcesValue * p0[number] * sumMultiplyN[number] / muIntensity
                series.data.add(XYChart.Data<Double, Double>(ro, eN[number]))
                number++;
            }
            chart.xAxis.label = "Приведена інтенсивність, ρ"
            chart.yAxis.label = "Середній час заявки в СМО, Т"
            chart.title = "Залежність СМО від наведеної інтенсивності"

            drawGraph(series)

        }

        if (tOtM.isSelected){

            val series = XYChart.Series<Double, Double>()

            var number = 0
            var multiply = 0.0

            muIntensity = lambdaIntensityValue / roIntensityValue
            for (m in 1..countOfSourcesValue){

                for (n in 1..m) {
                    multiply = 1.0
                    for (j in 0 until n) {
                        multiply *= Math.exp(lambdaIntensityValue * (m * tauValue + j / muIntensity)) - 1;
                    }
                    sumMultiplyNForP0[number] += multiply;
                }

                p0[number] = 1 / (1 + sumMultiplyNForP0[number]);

                for (n in 1 until m){
                    multiply = 1.0
                    for (j in 0..n){
                        multiply *= Math.exp(lambdaIntensityValue * (m * tauValue + j / muIntensity)) - 1;
                    }
                }
                sumMultiplyN[number] += multiply;

                eN[number] = countOfSourcesValue * tauValue + countOfSourcesValue * p0[number] * sumMultiplyN[number] / muIntensity;
                series.data.add(XYChart.Data<Double, Double>(m.toDouble(), eN[number]))
                number++;
            }

            chart.xAxis.label = "Кількість джерел заявок, m"
            chart.yAxis.label = "Середній час заявки в СМО, Т"
            chart.title = "Залежність СМО від кількості джерел заявок"

            drawGraph(series)
        }
    }

    fun drawGraph(series : XYChart.Series<Double, Double> ){
        chart.data.clear()
        chart.data.add(series)
    }
}


